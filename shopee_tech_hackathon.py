#
# Shopee Tech Hackathon 26/5/2020
# Apparently I cannot use the async version hence have to resort to brute-force requests
# Also Shopee changed their voucher api
#

import requests
import json
import sys

start=int(sys.argv[1])
end=int(sys.argv[2])

# Code 1 HACK
# Code 2 CODE
# Code 3
code=""

# Below requests code generated from Insomnia
url = "https://shopee.sg/api/v2/voucher_wallet/save_platform_voucher_by_voucher_code"
headers = {
  'cookie': 'SPC_IA=-1; SPC_EC="PbS8OhNoPd4Fgv3nNZ6zT2ESdszA/lDnu8ClBkIhqbqhC82M5CU9HVfGwYOPbxZJTQBx3m9ujcoPCfknZVG6USTzMmVem224uRUJ+s6gYinQ6WgRcpT8y+o/u6kOifxihhRclH5B/MLC3wwwuedrNfO85pGZByTL91sbaC99ic0="; SPC_F=kbTW9iwtX3MJCsfiSfEH78N9tImuUaP4; REC_T_ID=02128d66-604d-11e9-8629-f898efc711dd; SPC_T_ID="BZXAHpUsUtDdm0FoYYO/82aiwdW4OlkGYR0y4n7/vOncxZSWuNdLsyvtW1Is+TSmIpsTIvT3O2+NuWOYpQFQCvHD95w4dTcM/U6VQaTyzkw="; SPC_U=89168643; SPC_T_IV="ZKTtc9Eeaej6X4+/QyxNoA=="; SC_DFP=VOt0QoZRYwqqPLYvq512xP81wUkF9Isj; G_ENABLED_IDPS=google; SPC_R_T_ID="BZXAHpUsUtDdm0FoYYO/82aiwdW4OlkGYR0y4n7/vOncxZSWuNdLsyvtW1Is+TSmIpsTIvT3O2+NuWOYpQFQCvHD95w4dTcM/U6VQaTyzkw="; SPC_R_T_IV="ZKTtc9Eeaej6X4+/QyxNoA=="; _gcl_au=1.1.642051436.1590424522; SPC_SI=6uj0cnkopgesohtxw757jp6vhmgp35uw; _fbp=fb.1.1590424523499.738836518; _ga=GA1.2.1861041337.1590424525; _gid=GA1.2.1155322349.1590424525; cto_bundle=uvdnmV9qMk16WUtzNVNFbXljJTJGbk5Oc3NSU1l6QVJjVG8yWFB1UnRSSyUyQmFVZXp0N1JjU3U2anpNdzdYS05WdVBDbldQTWw5SEFvZDc2a3lzSnlqSU9YR1lzUk5rejhFVHRFeWZ1WkR4N285bmU5Y2JXTHp0RGFEdnVMb0xIR0VaVGgybjNQdURCcFh3dEV3aXBvUWh3TDdHQmpRJTNEJTNE; CTOKEN=6HUacp7fEeqp2PBj%2BRgRRQ%3D%3D; csrftoken=vUJe9pV5gs4Y2ABBU9LxdnLhZf4VKIeq; welcomePkgShown=true',
  'x-csrftoken': "vUJe9pV5gs4Y2ABBU9LxdnLhZf4VKIeq",
  'referer': 'https://shopee.sg/cart/'
}

payload = dict()
for i in range(start, end): 
  payload["voucher_code"] = f'{code}{i:04}'
  data = json.dumps(payload)
  response = requests.request("POST", url, data=data, headers=headers)

  resp = json.loads(response.text)
  if 'error_code' in data:
    print(f"Server too busy with code {i}")
  else:
    if "data" in resp and "voucher" in resp["data"] and resp["data"]["invalid_message_code"] != 8:
      print(f"I think it's {i:04}")
