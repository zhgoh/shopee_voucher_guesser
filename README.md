# Shopee Voucher Checker

This script is for one of the Shopee events where people are given a word hint and have to guess the 5 digits of the voucher to 
redeem some free stuff

Initially using requests and sequential programming, later revision, I have changed it to async to make it faster

## API

Initially I was figuring out how to find the API via my mobile app, I went through the hassle of decompiling my phone APK and
stuff and I wanted to use mitmproxy to find out the calls, eventually, I figured out that I can actually access through my
web browser. I used browser network tab to monitor the api calls to the voucher verification and found out the API used.

## asyncio

Request is a blocking call and do not allow async, therefore we need to use aiohttp library instead to take advantage of the
async features of Python.

## Voucher format

The voucher consist of two parts, the first part is a word was released at some fixed time and shoppers have to guess the second
part which consists of 5 random digits. To participate, you have to add the item to cart first and then type in the voucher in
the cart.
