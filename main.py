# Bot to check shopee valid voucher code
import json
import asyncio
import aiohttp
from typing import IO
from aiohttp import ClientSession

async def fetch_one(url:str, headers:dict, payload:dict, keyword:str, counter:int, session:ClientSession) -> None:
    """
    Change the payload of the http request and send
    """

    payload['promotion_data']['voucher_code'] = f'{keyword}{counter:05}'
    data = json.dumps(payload)

    try:
        response = await session.post(url=url, data=data, headers=headers)
    except (
        aiohttp.ClientError,
        aiohttp.http_exceptions.HttpProcessingError,
    ) as e:
        print(f"Error getting link for {counter}")

    html = await response.text()
    if html:
        res_json = json.loads(html)

        # Found the response with the correct code
        if res_json['invalid_message_reason'] != 'WRONG_CODE':
            item = res_json['voucher_code']
            print(f'{counter} is correct')


async def bulk_crawl(max_counter:int, url:str, headers:dict, payload:dict, keyword:int) -> None:
    """
    Collate all the fetch request and then start the tasks
    """

    async with ClientSession() as session:
        tasks = []

        # Create the tasks
        for counter in range(0, max_counter):
            tasks.append(
                fetch_one(url=url, headers=headers, payload=payload, keyword=keyword, counter=counter, session=session)
            )
        # Will wait for all tasks to complete
        await asyncio.gather(*tasks)


if __name__ == '__main__':
    # Making sure we are running compatible versions that support asyncio
    import sys
    assert sys.version_info >= (3, 7)

    # Based on shopee's part 1 keyword
    keyword = 'MATCH'

    # Based on shopee's range of numbers to guess from
    max_counter = 100000

    # Shopee api for checking coupons
    url = "https://shopee.sg/api/v2/checkout/coupon"
    headers = {
        "cookie": "SPC_IA=-1; SPC_EC=\"eQ3DsolCCKAU9dpZioVwMP7NzATuR4Mheu+hzvFAVzu8GHN1hwWUaVW/XlTQRqOFxTsLgbJh/qpqyBhxLTkmImyoicq5d2XC6iBjo8Kb6YpV3Rr6qtyBg7tymCBcFBuy8gdWQeusO4Bsx5o7CUGB74lIpLYzX6+Zu/ub9K8WI5Q=\"; SPC_F=kbTW9iwtX3MJCsfiSfEH78N9tImuUaP4; REC_T_ID=02128d66-604d-11e9-8629-f898efc711dd; SPC_T_ID=\"KiGArEfIRr641j+3PhYUgPgHuFBKtn/1fnkPDwZzvwcc32LYQIRa91ePVvJ9WV6EtYFkH8TIH8Hciol1TMNVZSQAmU8jVJuws/9vA+cp/Bo=\"; SPC_U=89168643; SPC_T_IV=\"DLJohD1NYj7WrFTdZTOdXw==\"; SC_DFP=VOt0QoZRYwqqPLYvq512xP81wUkF9Isj; G_ENABLED_IDPS=google; SPC_R_T_ID=\"KiGArEfIRr641j+3PhYUgPgHuFBKtn/1fnkPDwZzvwcc32LYQIRa91ePVvJ9WV6EtYFkH8TIH8Hciol1TMNVZSQAmU8jVJuws/9vA+cp/Bo=\"; SPC_R_T_IV=\"DLJohD1NYj7WrFTdZTOdXw==\"; csrftoken=vUJe9pV5gs4Y2ABBU9LxdnLhZf4VKIeq; SPC_SI=wyheos1fff7mqnyzppzjqj6ijf0aq2pe; REC_MD_20=1585299878; welcomePkgShown=true; REC_MD_30_2001269921=1585300418; REC_MD_41_1000021=1585300118_0_50_0_37; CTOKEN=2i1l33AJEeqBmsy7%2Fl34WA%3D%3D",
        "x-csrftoken": "vUJe9pV5gs4Y2ABBU9LxdnLhZf4VKIeq",
        "referer": "https://shopee.sg/cart/"
    }

    with open('payload.json', 'rt') as in_file:
        payload = in_file.read()
        payload = json.loads(payload)

    if payload:
        asyncio.run(bulk_crawl(max_counter=max_counter, url=url, headers=headers, payload=payload, keyword=keyword))
